const Router = require('express').Router;
const userController = require('../controllers/user-controller');
const router = new Router();
const { body } = require('express-validator');
const authMiddleware = require('../middlewares/auth-middleware');

router.post('/registration',
    body('phone').isMobilePhone(),
    body('password').isLength({ min: 6, max: 32 }),
    body('username').isLength({ min: 5, max: 12 }),
    userController.registration);
router.post('/login',
    // body('phone').isMobilePhone(),
    // body('password').isLength({ min: 6, max: 32 }),
    userController.login);
router.post('/logout', userController.logout);
router.post('/activate/', userController.activate);
router.get('/refresh', userController.refresh);
router.get('/users',authMiddleware, userController.getUsers);
// router.get('/post',authMiddleware, userController.getUsers);



module.exports = router;
