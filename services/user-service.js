const UserModel = require('../models/user-model');
const phoneService = require('../services/phone-service');
const tokenService = require('../services/token-service');
const UserDto = require('../dtos/user-dto');
const bcrypt = require('bcrypt');
const ApiError = require('../exceptions/api-error');


class UserService {
    async registration(phone,username,password) {
        const candidate = await UserModel.findOne({phone});
        if(candidate) {
            throw ApiError.BadRequest(`The user with ${phone} already exists`)
        }
        const hashPassword = await bcrypt.hash(password, 3);
        //send activation code 

        const user = await UserModel.create({phone,username: username,password: hashPassword});
        await phoneService.sendActivationCode(phone);
        const userDto = new UserDto(user); 
        const tokens = tokenService.generateTokens({...userDto});
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return {
            ...tokens, 
            user:userDto
        }
    }
    
    async login(phone, password) {
        const user = await UserModel.findOne({phone});
        if(!user) {
            throw ApiError.BadRequest('The user was not found');
        }
        const isPasswordCorrect = await bcrypt.compare(password, user.password);
        if(!isPasswordCorrect) {
            throw ApiError.BadRequest('Incorrect password');
        } 
        const userDto = new UserDto(user);
        const tokens = tokenService.generateTokens({...userDto});
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return {
            ...tokens, 
            user:userDto
        } 
    }

    async verifyCode(phone, code) {
        const user = await UserModel.findOne({phone});
        console.log(user);
        const data = await phoneService.verifyActivationCode(phone, code);
        if(!user) {
            throw ApiError.BadRequest('Incorrect user');
        }
        if(data.status === "approved") {
            user.isActivated = true;
            await user.save();
        } else {
            throw ApiError.BadRequest('Something went wrong');
        }
    }

    async logout(refreshToken) {
         const token = await tokenService.deleteToken(refreshToken);
         return token;
    }

    // async activate(activationCode) {
    //     const user = await UserModel.findOne({activa})
    // }


    async refresh(refreshToken) {
        if (!refreshToken) {
            throw ApiError.UnauthorizedError();
        }
        const userData = tokenService.validateRefreshToken(refreshToken);
        const tokenFromDb = await tokenService.findToken(refreshToken);
        if (!userData || !tokenFromDb) {
            throw ApiError.UnauthorizedError();
        }
        const user = await UserModel.findById(userData.id);
        const userDto = new UserDto(user);
        const tokens = tokenService.generateTokens({...userDto});

        await tokenService.saveToken(userDto.id, tokens.refreshToken);
        return {...tokens, user: userDto}
    }

    async getAllUsers() {
        const users = await UserModel.find();
        return users;
    }
}

module.exports = new UserService();