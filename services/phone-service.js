const client = require('twilio')(process.env.ACCOUNT_SID,process.env.AUTH_TOKEN);

class PhoneService {
    async sendActivationCode(usersNumber) {
        await client.verify.services(process.env.SERVICE_ID).verifications.create({
            to:`+${usersNumber}`,
            channel:'sms'
        });
    }

    async verifyActivationCode(usersNumber, verificationCode) {
       const data = await client
        .verify
        .services(process.env.SERVICE_ID)
        .verificationChecks
        .create({
            to: `+${usersNumber}`,
            code: verificationCode
        });
        return data;
    }
}

module.exports = new PhoneService();