const {Schema, model} =require('mongoose');

const UserSchema = new Schema({
    phone:{type:String, unique:true,required:true},
    username:{type:String, unique:true,required:true},
    password:{type:String,required:true},
    isActivated:{type:Boolean,default:false,},
    nativeLanguage:{type:String,},    
    wantedLanguage:{type:String,},    
    verificationCode:{type:String,},
})

module.exports = model('User',UserSchema);